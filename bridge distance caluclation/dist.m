clear all
close all
clc

%Long bridge case (l_bridge> 2*8.3*7.5 = 124 meters - as accident would happen in the middle of bridge -refer diagram)
t=3.5;  %value found such that distance_case1=100 meters (between V1 and V2)
h=14; %assuming P5 as (0,0,h); B1=(0,0,0);

x= 33 + 8.3*t;
y = 30 +13.8*t;

%P_Alpha_prime=(x,0,h); corresponds to V1
%B_2_prime=(0,y,0); corresponds to V2


distance_case1 = sqrt(x^2+y^2+h^2);

%Short bridge case(total bridge length - 43 meters, accident spot = 20 meters(middle of bridge))
t_case2 = 3.5; %value found such that distance_case2=100 meters (between V1 and V2)

x_1=32.8+8.3*t_case2*cosd(10);
y_2= 30 + 13.8*t_case2  ;
z_1=11.7-8.3*t_case2*sind(10);

%P_Alpha_prime_case2=(x_1,0,z_1); corresponds to V1
%B_2_prime_case2=(0,y_1,0); corresponds to V2

distance_case2 = sqrt(x_1^2+y_2^2+z_1^2);

%Displacement calculation:
thetha= 10;%slope of the bridge to the ground
v_bridge=30; %permitted maximum velocity on bridge in kmph
t_msg= 0.1; %frequency of messages 10 Hz
v_V1= v_bridge* 5/18;%velocity in meters per second (of car V1)
v_V1_ver= v_V1 * sind(thetha); %vertical velocity of V1 in meter per sec (whilst on slope)
v_V1_hor= v_V1 * cosd(thetha); %Horizontal velocity of V1 in meter per sec (whilst on slope)

d_V1_ver= v_V1 * sind(thetha) * t_msg; %vertical distance covered per message by V1 (whilst on slope)
d_V1_hor= v_V1 * cosd(thetha) * t_msg; %Horizontal distance covered by V1 per message(whilst on slope)

