% clear everything
close all
clear all
clc

% % Example Transfer Function: G(s) = 1+ 0.3 s/( s^2 + 0.3 s + 1)
% 
% % Numerator
% num = [0.6 1];
% % Denominator; to feed value of highest power first 
% den = [1 0.6 1];
% 
% % Transfer Function
% G = tf(num, den)
% 
% % Plot Frequency Response
% bode(G), grid 

% Example Transfer Function: # 73, eq. 6.25

% num = [1 q1 ];
% % Denominator; to feed value of highest power first 
% den = [1+q3 q1+q4];

t = 0.5
k1=0.15
k2=-0.1





% Transfer Function - G = tf(num,den)
G = tf([ -0.03 1], [0.08 0.05 1 ])  % note - max. power of s first...

%% % Plot Frequency Response
bode(G), grid 

%root locus
%rlocus(G)
% RLocusGui(G)


% %plot step response
% step(G)