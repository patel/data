########################################################
# Computation of mean speed (all nodes) for each second#
########################################################

# Store the time when the packet is sent
BEGIN {
        nodes=10
        lessnodes = nodes - 1
	i=0;
	entry=0
}
{
	if(i % nodes == 0) {
		now = $2
		time[i]= $2
	}
	speed[now]+= $5
	if (i % nodes == lessnodes){
		mean_speed[entry]= speed[now]/nodes
		entry++
	}
	i++
}
END {
for (y=0;y<entry;y++) {
    		print mean_speed[y]
	}
}
