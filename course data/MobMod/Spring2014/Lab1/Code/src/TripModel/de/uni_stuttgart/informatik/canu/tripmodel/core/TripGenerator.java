package de.uni_stuttgart.informatik.canu.tripmodel.core;

/**
 * <p>Title: Trip Model</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002-2003</p>
 * <p>Company: University of Stuttgart</p>
 * @author Illya Stepanov
 * @version 1.1
 */

import de.uni_stuttgart.informatik.canu.mobisim.core.Node;

/**
 * This interface is to generate a new node's trip
 * @author Illya Stepanov 
 */
public interface TripGenerator
{
  
	/**
   * The mean for the Poisson Random Generator. <br>
   * <br>
   */
/*	public double poissonMean;*/
	
	/**
   * Set the mean of the Poisson Random Generator. <br>
   * <br>
   * @param beta poisson parameter
   */
 /* public void setPoissonMean(double beta);*/
	
	/**
   * Return a random number with Poisson distribution. <br>
   * <br>
   * @return poisson random variable
  */
 /* public double getPoissonRandom();*/
		
	/**
   * Generates a new node's trip. <br>
   * <br>
   * @param node node
   * @return new node's trip
   */
  public Trip genTrip(Node node);

  /**
   * Chooses time of node staying at the current position. <br>
   * <br>
   * @param node node
	 * @param poisson - true if StayDuration is Poisson distributed
	 *                - false if StayDuration is Uniformly distributed
   * @return stay duration (in ms)
   */
  public int chooseStayDuration(Node node);
}