package de.uni_stuttgart.informatik.canu.spatialmodel.geometry;

/**
 * <p>Title: Spatial Model</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: University of Stuttgart</p>
 * @author Illya Stepanov
 * @version 1.1
 * <p>Patches: </p>
 * <p> <i> Version 1.2 by Marco Fiore (fiore@tlc.polito.it) on 06/12/2005:
 * <br> &nbsp;&nbsp;&nbsp;&nbsp; Added length computation function </i> </p>
 */

/**
 * This class implements a line connecting several points
 * @author 1.0-1.1 Illya Stepanov
 * @author 1.2 Marco Fiore
 * @version 1.2
 */
public class Polyline extends GeometryElement
{
  /**
   * Array of points
   */
  protected java.util.ArrayList points = new java.util.ArrayList();

  /**
   * Constructor
   */
  public Polyline()
  {
  }

  /**
   * Gets the array of points. <br>
   * <br>
   * @return array of points
   */
  public java.util.ArrayList getPoints()
  {
    return points;
  }

  /**
   * Checks if this geometry element contains a given element. <br>
   * <br>
   * @param e element being checked
   */
  public boolean contains(GeometryElement e)
  {
    // polyline can contain only point, line or polyline
    if (e instanceof Point)
    {
      Point point = (Point)e;
      // check if one of the lines of polyline contains this point
      for (int i=0; (i+1)<points.size(); i++)
      {
        Line line = new Line((Point)points.get(i), (Point)points.get(i+1));
        if (line.contains(point))
          return true;
      }

      return false;
    }
    else
    if (e instanceof Polyline)
    {
      // check if every point of polyline belongs to this polyline
      Polyline line = (Polyline)e;
      java.util.Iterator iter = line.getPoints().iterator();
      while (iter.hasNext())
      {
        Point p = (Point)iter.next();
        if (!contains(p))
          return false;
      }

      return true;
    }

    return false;
  }

  /**
   * Gets the closest line segment to a point. <br>
   * <br>
   * @param p point
   * @return closest line segment to a point
   */
  public Line getClosestSegment(Point p)
  {
    Line line = null;
    double minDist = Double.MAX_VALUE;

    // iterate all the lines
    for (int i=0; i<points.size()-1; i++)
    {
      Line tempLine = new Line((Point)points.get(i), (Point)points.get(i+1));
      if (tempLine.contains(p))
        return tempLine;

      double f = tempLine.getDistance(p);
      if (f<minDist)
      {
        line = tempLine;
        minDist = f;
      }
    }

    return line;
  }

  /**
   * Compute length of polyline. <br>
   * <br>
   * @return length of polyline
   */
  public double getLength()
  {
    double length = 0.0f;
		Point currPoint, oldPoint = null;
    //double minDist = Double.MAX_VALUE;

    // iterate all the lines
    for (int i=0; i<points.size(); i++)
    {
			currPoint = (Point)points.get(i);

      if(oldPoint != null) {
        length += oldPoint.getDistance(currPoint);
      }

      oldPoint = currPoint;
    }

    return length;
  }
}
