package eurecom.corsim.records;

public class NodeRecord {

  private String ID = null;
	private int X = 0;
	private int Y = 0;
	
  public NodeRecord(String ID, int X, int Y) {
	  this.ID = ID;
		this.X=X;
		this.Y=Y;
	}
	
	public String getID() {
	  return ID;
	}
	
	public String getX() {
		String XValueFeetString = String.valueOf(X);
		return (XValueFeetString.substring(Math.max(0,XValueFeetString.length()-9),XValueFeetString.length()-1));
	}
	
	public String getY() {
		String YValueFeetString = String.valueOf(Y);
		return (YValueFeetString.substring(Math.max(0,YValueFeetString.length()-9),YValueFeetString.length()-1));
	}					
}
