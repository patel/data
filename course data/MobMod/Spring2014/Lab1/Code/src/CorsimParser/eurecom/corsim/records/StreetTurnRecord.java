package eurecom.corsim.records;

public class StreetTurnRecord {

  private String from = null;
	private String to = null;
	private int straight = 0;

  public StreetTurnRecord(String from, String to) {
	  this.from = from;
		this.to = to;
		this.straight = 100;
	}
	
	public String getFrom() {
	  return from;
	}
	
	public String getTo() {
	  return to;
	}
	
	public String getStraight() {
	  return (String.valueOf(straight));
	}
}
