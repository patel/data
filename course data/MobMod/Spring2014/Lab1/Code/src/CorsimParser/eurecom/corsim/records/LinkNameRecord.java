package eurecom.corsim.records;

public class LinkNameRecord {

  private String from = null;
	private String to = null;
	private String name = null;

  public LinkNameRecord(String from, String to, String name) {
	  this.from = from;
		this.to = to;
		this.name = name.substring(0,11);
	}
	
	public String getFrom() {
	  return from;
	}
	
	public String getTo() {
	  return to;
	}
	
	public String getName() {
	  return name;
	}
}
