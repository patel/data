package eurecom.corsim.records;

public class LinkRecord {

  private String from = null;
	private String to = null;
	private int length = 0;
	private int numberLane = 0;
	private int grade = 0;
	private int queueDischarge = 0;
	private int meanStartUpLost = 0;
	private int meanQueueDischarge = 0;
	private int meanSpeed = 0;
	private int	pedestrians = 0;
	
  public LinkRecord(String from, String to, int length, int numberLane) {
	  this.from = from;
		this.to = to;
		this.length = length;	
		this.numberLane = numberLane;
		grade = 0;
		queueDischarge = 1;
		meanStartUpLost = 20;
		meanQueueDischarge = 18;
		meanSpeed = 30;
		pedestrians = 0;
	}
		
	public String getFrom() {
	  return from;
	}
	
	public String getTo() {
	  return to;
	}
	
	public String getLength() {
	  return (String.valueOf(length));
	}
	
	public String getNumberLane() {
	  return (String.valueOf(numberLane));
	}
	
	public String getGrade() {
	  return (String.valueOf(grade));
	}
	
	public String getQueue() {
	  return (String.valueOf(queueDischarge));
	}
	
	public String getMeanStartLost() {
	  return (String.valueOf(meanStartUpLost));
	}
	
	public String getMeanQueue() {
	  return (String.valueOf(meanQueueDischarge));
	}
	
	public String getMeanSpeed() {
	  return (String.valueOf(meanSpeed));
	}
	public String getPedestrians() {
	  return (String.valueOf(pedestrians));
	}
}
