package eurecom.corsim.core;

/**
 * <p>Title: GDF Writer</p>
 * <p>Description: Parse a Statial Model to CORSIM .TRF files </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Eurecom Institute</p>
 * @author Jerome Haerri
 * @version 1.0
 */

import java.io.*;
import de.uni_stuttgart.informatik.canu.mobisim.core.*;
import de.uni_stuttgart.informatik.canu.mobisim.notifications.*;
import de.uni_stuttgart.informatik.canu.spatialmodel.core.*;
import de.uni_stuttgart.informatik.canu.spatialmodel.geometry.*;

import eurecom.corsim.records.*;

/**
 * This class saves the contents of spatial model in a CORSIM data file. <br>
 * <br>
  * @author Jerome Haerri
	* @version 1.0
 */
public class CORSIMParser extends ExtensionModule {
  /**
   * Output Stream
   */
  protected java.io.PrintStream o = System.err;
	
	/**
   * Output Stream
   */
	protected java.io.PrintWriter out = null;
	
	 /**
   * Source stream
   */
  protected BufferedReader source;
	
	/**
	* Input Stream
	* @since 1.0
	*/
	protected java.io.InputStream is =null;
	
	/**
   * Text from current record
   */
  protected java.util.ArrayList lines = new java.util.ArrayList();
	
  /**
   * Index of next symbol in lines[0] under analysis
   */
  protected int ind;
  
	/**
   * Record String Buffer
   */
  private StringBuffer buff=null;
	
	/**
   * Record String Buffer Arrays
   */ 
	private java.util.ArrayList buffArray = new java.util.ArrayList();
	
	/**
   * Offset marking the position in the Record String Buffer
   */ 
	private int offset = 0;
	
	/**
   * CORSIM Link Name Records (10)
   */ 
	private java.util.ArrayList linkNameRecords = new java.util.ArrayList();
	
	/**
   * CORSIM Link Records (11)
   */
	private java.util.ArrayList linkRecords = new java.util.ArrayList();
	
	/**
   * CORSIM Node Record (195)
   */
	private java.util.ArrayList nodeRecords = new java.util.ArrayList();
	
	/**
   * CORSIM Traffic Record (35)
   */
	private java.util.ArrayList trafficRecords = new java.util.ArrayList();
	
	/**
   * CORSIM Traffic Code Record (36)
   */
	private java.util.ArrayList trafficCodeRecords = new java.util.ArrayList();
	
		/**
   * CORSIM Traffic Code Record (21)
   */
	private java.util.ArrayList streetTurnRecords = new java.util.ArrayList();

	
  /**
   * Spatial Model
   */
  protected SpatialModel spatialModel;
	
	
	protected java.util.Map pointsIDMapping = null;
  
  /**
   * Constructor
   */
  public CORSIMParser() {
    super("CORSIMParser");
  }

  /**
   * Returns the module's description. <br>
   * <br>
   * @return extension module's description
   */
  public String getDescription() {
    return "CORSIM Parser module";
  }

	 /**
   * Returns the Universe reference. <br>
   * <br>
   * @return u
   */
	protected Universe getUniverse() {
	  return u;
	}
  /**
   * Executes the extension. <br>
   * <br>
   * The method is called on every simulation timestep.
   * @return 0 - the module should be executed on next timesteps,
   *        -1 - the module should not be executed on further timesteps and should be removed from the extensions' list
   */
  public int act() {
		try {
			pointsIDMapping = new java.util.HashMap();
			spatialModel.rebuildGraph();
			doSave();
    }
		catch (Exception e) {
		  System.out.println("Exception in CORSIMParser.act()");
			e.printStackTrace();
		}
    return -1;
  }
  
  /**
   * Saves the contents of spatial model in GDF format
   */
  public void doSave() {

		try {
			//loadBaseFile();
			createBase();
			writeCORSIM();
			completeRecord();
			//writeCommentRecord("This volume has been automatically generated by the GDFGenerator from Newcom NoE. contact: Jerome Haerri <haerri@ieee.org>");
		}
		
		catch (Exception e) {
		  System.out.println("Exception in CORSIMParser.doSave()");
			e.printStackTrace();
			System.exit(-1);
		}
  }
  
	protected void writeCORSIM() {
	  java.util.Map elements = spatialModel.getElements();
		
		java.util.Iterator iter = elements.values().iterator();
		
		try {
			while (iter.hasNext()) {
				SpatialModelElement element = (SpatialModelElement)iter.next();
				if (element.getClassCode().equals("41")) {
					if (element.getSubClassCode().equals("20")) {
						// junctions  
						//JHnote: TBD
					}
					else {
						if (element.getSubClassCode().equals("10")){
							// roadElements: points + 
							createLineFeatureRecord(element);
						}
					}
				}
			}
			writeRecords(); 
		}
		catch (Exception e) {
		  System.out.println("Exception in CORSIMParser.writeCORSIM()");
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	protected void writeRecords() {
		//writeInitialRecord();
	  writeLinkNameRecord();
		writeLinkRecord();
		writeStreetTurnRecord();
		writeTrafficRecord();
		writeTrafficCodeRecord();
		writeSubNetworkRecord();
		writeNodeRecord();
		//writeEndRecord();
	}
	
	
	protected void writeLinkNameRecord() {
		try {
			for (int i = 0; i < linkNameRecords.size(); i++) {
				LinkNameRecord record = (LinkNameRecord)linkNameRecords.get(i);
				createRecord();
				writeRecord(record.getFrom(),4);
				writeRecord(record.getTo(),4);
				writeRecord(record.getName(),12);
				saveRecordType(10);
				packRecord();
			}
		}
		catch (Exception e) {
			System.out.println("Exception in CORSIM.writeLinkNameRecord: Cannot write record");
			e.printStackTrace();
			System.exit(-1);
		}	
	}
	
	protected void writeLinkRecord() {
		try {
			for (int i = 0; i < linkRecords.size(); i++) {
				LinkRecord record = (LinkRecord)linkRecords.get(i);
				createRecord();
				writeRecord(record.getFrom(),4);
				writeRecord(record.getTo(),4);
				writeRecord(record.getLength(),8,4);
				writeRecord(record.getNumberLane(),21,1);
				writeRecord(record.getGrade(),26,2);	
				writeRecord(record.getQueue() ,28,1);
				writeRecord(record.getMeanStartLost(),56,4);
				writeRecord(record.getMeanQueue(),60,4);
				writeRecord(record.getMeanSpeed(),64,4);
				writeRecord(record.getPedestrians(),70,1);
				saveRecordType(11);
				packRecord();
			}
		}
		catch (Exception e) {
			System.out.println("Exception in CORSIM.writeLinkRecord: Cannot write record");
			e.printStackTrace();
			System.exit(-1);
		}	
	}
	
	protected void writeStreetTurnRecord() {
		try {
			for (int i = 0; i < streetTurnRecords.size(); i++) {
				StreetTurnRecord record = (StreetTurnRecord)streetTurnRecords.get(i);
				createRecord();
				writeRecord(record.getFrom(),4);
				writeRecord(record.getTo(),4);
				writeRecord(record.getStraight(),16,4);
				saveRecordType(21);
				packRecord();
			}
		}
		catch (Exception e) {
			System.out.println("Exception in CORSIM.writeStreetTurnRecord: Cannot write record");
			e.printStackTrace();
			System.exit(-1);
		}	
	}
	
	protected void writeTrafficRecord() {
		try {
			for (int i = 0; i < trafficRecords.size(); i++) {
				TrafficRecord record = (TrafficRecord)trafficRecords.get(i);
				createRecord();
				writeRecord(record.getFrom(),0,4);
				writeRecord(record.getTo(),8,4);
				saveRecordType(35);
				packRecord();	
			}
		}
		catch (Exception e) {
			System.out.println("Exception in CORSIM.writeTrafficRecord: Cannot write record");
			e.printStackTrace();
			System.exit(-1);
		}	
	}
	
	protected void writeTrafficCodeRecord() {
		try {
			for (int i = 0; i < trafficCodeRecords.size(); i++) {
				TrafficCodeRecord record = (TrafficCodeRecord)trafficCodeRecords.get(i);
				createRecord();
				writeRecord(record.getID(),0,4);
				writeRecord(record.getCode(),5,1);
				saveRecordType(36);
				packRecord();	
			}
		}
		catch (Exception e) {
			System.out.println("Exception in CORSIM.writeTrafficCodeRecord: Cannot write record");
			e.printStackTrace();
			System.exit(-1);
		}	
	}
	
	protected void writeSubNetworkRecord() {
		try {
			createRecord();
			writeRecord("0",0,1);
			saveRecordType(170);
			packRecord();	
		}
		catch (Exception e) {
			System.out.println("Exception in CORSIM.writeSubNetworkRecord: Cannot write record");
			e.printStackTrace();
			System.exit(-1);
		}	
	}
	
	protected void writeNodeRecord() {
		try {
			for (int i = 0; i < nodeRecords.size(); i++) {
				NodeRecord record = (NodeRecord)nodeRecords.get(i);
				createRecord();
				writeRecord(record.getID(),4);
				writeRecord(record.getX(),8);
				writeRecord(record.getY(),8);
				saveRecordType(195);
				packRecord();		
			}
		}
		catch (Exception e) {
			System.out.println("Exception in CORSIM.writeNodeRecord: Cannot write record");
			e.printStackTrace();
			System.exit(-1);
		}	
	}
	
	protected void createLineFeatureRecord(SpatialModelElement element) throws Exception {
	  java.util.Random rand=u.getRandom();
		Polyline shape = (Polyline)element.getGeometry();
		java.util.ArrayList points = shape.getPoints();
		double feet = 0.3049;	
		try {
			
			Point pointf = (Point)points.get(0); // initial point
			Point pointt = (Point)points.get(points.size()-1); // end point. 
			
			String IDFrom= (String)pointsIDMapping.get(pointf.getID());
			String IDTo= (String)pointsIDMapping.get(pointt.getID());
			
			boolean addedTo = false;
			boolean addedFrom = false;
			
			if(IDFrom == null ) {
				
					IDFrom = Integer.toString(rand.nextInt(6999));
					pointsIDMapping.put(pointf.getID(),IDFrom);
					double XValueFeet = pointf.getX()/feet;
					double YValueFeet = pointf.getY()/feet;
					
					// adding the node record
					NodeRecord nodeRecord = new NodeRecord(IDFrom,(int)XValueFeet,(int)YValueFeet);
					nodeRecords.add(nodeRecord);
					
					
					
					addedFrom = true;
			}
			/*else {
			  IDFrom= (String)pointsIDMapping.get(pointf.getID());
			}*/
		
			if(IDTo == null ) {
				
				IDTo = Integer.toString(rand.nextInt(6999));
				pointsIDMapping.put(pointt.getID(),IDTo);
				
				double XValueFeet = pointt.getX()/feet;
				double YValueFeet = pointt.getY()/feet;
				
				// adding the node record
				NodeRecord nodeRecord = new NodeRecord(IDTo,(int)XValueFeet,(int)YValueFeet);
				nodeRecords.add(nodeRecord);
				
				addedTo = true;
			}
			/*else {
			  IDTo= (String)pointsIDMapping.get(pointt.getID());
			}*/
			
			if (addedFrom) {
			  // adding the default traffic lights for the second intersection
				TrafficRecord trafficRecord = new TrafficRecord(IDFrom,IDTo);
				trafficRecords.add(trafficRecord);	
				
				// adding the default traffic light code (1=green ball)
				TrafficCodeRecord trafficCodeRecord = new TrafficCodeRecord(IDFrom);
				trafficCodeRecords.add(trafficCodeRecord);
			}
			
			if (addedTo) {			
				// adding the default traffic lights for the second intersection
				TrafficRecord trafficRecord = new TrafficRecord(IDTo,IDFrom);
				trafficRecords.add(trafficRecord);
				
				// adding the default traffic light code (1=green ball)
				TrafficCodeRecord trafficCodeRecord = new TrafficCodeRecord(IDTo);
				trafficCodeRecords.add(trafficCodeRecord);
			}
			
			// adding default roadSegment parameters second direction (bi-directional road)
			String name = (String)element.getAttributes().get("RN");
			if (name ==null)
				name = " ";
				
			LinkNameRecord linkNameRecord = new LinkNameRecord(IDTo,IDFrom,name);
			linkNameRecords.add(linkNameRecord);
				
			
			String dirFlow = (String)element.getAttributes().get("DF");
			int dirFlowInt = 1;
			int roadLength = 0;
			
			
			String numberLane = null;
			
			if(dirFlow != null)
				dirFlowInt = Integer.parseInt(dirFlow);
			
			else
				dirFlowInt = 1;
			
			if((dirFlowInt == 1) || (dirFlowInt == 2) ){
				// adding default roadSegment parameters first direction
			 
				roadLength = (int)(Math.sqrt(Math.pow(pointf.getX()-pointt.getX(),2)+Math.pow(pointf.getY()-pointt.getY(),2))/feet);
			
				if(roadLength > 9999)
					throw new Exception("RoadSegment Length longer than 9999 feet");
			
				numberLane = (String)element.getAttributes().get("NL");
				if (numberLane !=null) {
					if(Integer.parseInt(numberLane)>7)
						throw new Exception("Number of lanes exceeds 7");
				}
				else
					numberLane = "1";
			
			 LinkRecord linkRecord = new LinkRecord(IDFrom,IDTo,roadLength,Integer.parseInt(numberLane));
			 linkRecords.add(linkRecord);
			 
			 StreetTurnRecord streetTurnRecord = new StreetTurnRecord(IDFrom,IDTo);
			streetTurnRecords.add(streetTurnRecord); 
			 //System.out.println("entering here with size of link record "+linkRecords.size());	
			}
			
			else if((dirFlowInt == 1) || (dirFlowInt == 3) ){
				
				 //System.out.println("entering here with size of link name record "+linkNameRecords.size());	
			
				numberLane = (String)element.getAttributes().get("NL");
				if (numberLane !=null) {
					if(Integer.parseInt(numberLane)>7)
						throw new Exception("Number of lanes exceeds 7");
				}
				else
					numberLane = "1";
			
				 LinkRecord linkRecord = new LinkRecord(IDTo,IDFrom,roadLength,Integer.parseInt(numberLane));
				 linkRecords.add(linkRecord);
				 
				 StreetTurnRecord streetTurnRecord = new StreetTurnRecord(IDTo,IDFrom);
				 streetTurnRecords.add(streetTurnRecord); 
				 //System.out.println("entering here with size of link record "+linkRecords.size());	
			}
		}
		catch (Exception e) {
			System.out.println("Exception in CORSIM.writeLineFeatureRecord: Cannot write record");
			e.printStackTrace();
			System.exit(-1);
		}	
	}
	
	
	protected void createBase() {
	  try {
			createRecord();
			writeRecord("Newcom NoE and UCLA",0,36);
			writeRecord("09",38,2);
			writeRecord("29",42,2);
			writeRecord("2006",44,4);
			writeRecord("Eurecom",48,23);
			writeRecord("0",72,4);
			saveRecordType(1);
			packRecord();		
			
			createRecord();
			writeRecord("1",6,2);
			writeRecord("0",11,1);
			writeRecord("0",15,1);
			writeRecord("3",16,4);
			writeRecord("7981",21,8); //seed
			writeRecord("00",30,2); // fuel emmission
			writeRecord("00",32,2);// fuel emmission options
			writeRecord("0",36,1);
			writeRecord("3",51,1); // Network subformat: 3=NetSim 8=FreeSim
			writeRecord("1154",52,4);
			writeRecord("7781",60,8); // seed for traffic generation
			writeRecord("7581",68,8); // seed for traffic decision
			saveRecordType(2);
			packRecord();		
		
			createRecord();
			writeRecord("200",0,4);
			saveRecordType(3);  // simulation time
			packRecord();		
		
			createRecord();
			writeRecord("60",16,4); // simulation time step
			saveRecordType(4);
			packRecord();		
		
			createRecord();
			writeRecord("0",0,4);
			writeRecord("0",8,4);
			writeRecord("0",12,4);
			writeRecord("0",20,4);
			writeRecord("0",24,4);
			writeRecord("0",28,4);
			writeRecord("0",32,4);
			writeRecord("0",36,4);
			writeRecord("0",40,4);
			writeRecord("0",47,1);
			saveRecordType(5);
			packRecord();		
		}
		catch (Exception e) {
		  System.out.println("Exception in CORSIMParser.writeCORSIM()");
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	protected void loadBaseFile() throws Exception {	
		String baseFile = "base"+".trf";
		try {
			source = new BufferedReader(new InputStreamReader(new FileInputStream(baseFile), "ISO-8859-1"));
		}
		catch (java.io.IOException ioe) {
			System.out.println("Cannot load the CORSIM base file. Please check that base.trf is in the active directory");
      ioe.printStackTrace();
    }
		finally {
			String s = null;
			
			 while ((s = source.readLine())!=null) {
				try {
					createRecord();
				  writeRecord(s,80);
				}
				catch (Exception e) {
					System.out.println("Exception in CORSIM.loadBaseFile: Cannot write record :"+s);
					e.printStackTrace();
					System.exit(-1);
				}
				finally {
				  packRecord();
				}
			}
		}
	}
	
	protected void completeRecord() throws Exception {
		
		String baseFile = "end"+".trf";
		try {
			source = new BufferedReader(new InputStreamReader(new FileInputStream(baseFile), "ISO-8859-1"));
		}
		catch (java.io.IOException ioe) {
			System.out.println("Cannot load the CORSIM end file. Please check that end.trf is in the active directory");
      ioe.printStackTrace();
    }
		finally {
			String s = null;
			 while ((s = source.readLine())!=null) {
				try {
					createRecord();
				  writeRecord(s,80);
				}
				catch (Exception e) {
					System.out.println("Exception in CORSIM.loadBaseFile: Cannot write record :"+s);
					e.printStackTrace();
					System.exit(-1);
				}
				finally {
				  packRecord();
				}
			}
		}
	}
	
	/**
   * Gets the next field of currently read record. <br>
   * <br>
   * Gets next field of currently read record.
   * Set size to -1 to get the rest of current line
   * @param size size of the field (in symbols)
   * @return field value or null if eof reached
   */
  protected String getNextField(int size) throws Exception {
    String s;
    // get next field from the record
 
    if (lines.size()==0)
       return null;
		
		s = (String)lines.get(0);
		
    s = s.substring(ind, ind+size);
    ind+=size;

    return s;
  }

	
	/**
   * Loads  base CORSIM records
   */
  protected void loadRecord() throws Exception {
    // clear buffer
    ind = 0;
    lines.clear();

    // mark the beginning of the record
    source.mark(8192);

    String s;
		
    // load next record
    while ((s = source.readLine())!=null) {
			String tempString = s.substring(78, 80);
			System.out.println("record type");
			lines.add(s);
		}
	}
	
	/** 
	 * Creates a record String Buffer and add it to the Array of Record String Buffer
	 *
	 */
	public void createRecord() throws Exception {
	  buff = new StringBuffer(80);
		buff.setLength(80);
		
		buffArray.add(buff);
		offset = 0;
	}
	
	public void saveRecordType(int recordType) throws Exception {
	  String recordTypeString = String.valueOf(recordType);
		//System.out.println("record type is "+recordType+"of length "+recordTypeString.length());
		if(recordTypeString.length() == 1) { // can only be 1,2 or 3
			buff.insert(79, recordType); 
		}
		else if (recordTypeString.length() == 2) { // can only be 1,2 or 3
			buff.insert(78, recordType); 
		}
		else {
			buff.insert(77, recordType); 
		}
		//System.out.println("buffer is "+buff);
	}
	
	/** 
	 * Creates a record String Buffer and add it to the Array of Record String Buffer
	 *
	 */
	public void createRecord(int recordType) throws Exception {
	  buff = new StringBuffer(80);
		buff.setLength(80);
		String recordTypeString = String.valueOf(recordType);
		//System.out.println("record type is "+recordType+"of length "+recordTypeString.length());
		if(recordTypeString.length() == 1) { // can only be 1,2 or 3
			buff.insert(80, recordType); 
		}
		else if (recordTypeString.length() == 2) { // can only be 1,2 or 3
			buff.insert(79, recordType); 
		}
		else {
			buff.insert(78, recordType); 
		}
		//System.out.println("buffer is "+buff);
		buffArray.add(buff);
		offset = 0;
	}

	/** 
	 * Writes the Record Buffers to the GDF File
	 * <br>
	 * Once the complete record has been parsed, one or several (continuation records) string buffers may have been 
	 * created. packRecord() simply print them in the provided output File.
	 *
	 */
	public void packRecord(){
	 for (int i = 0; i < buffArray.size(); i++) {
      StringBuffer localBuffer = (StringBuffer)buffArray.get(i);
		
			//localBuffer.setLength(80);
			
			//out.println("Trying to print");
			
			String stringRecord = localBuffer.toString();
			String stringReady = stringRecord.replaceAll("\u0000"," ");
			
			//System.out.println(localBuffer.toString());
			//o.println(localBuffer.toString());
		
			//o.println(localBuffer);
			//o.println(stringRecord);
				o.println(stringReady);
		}
		buffArray.clear();
		buff = null;
		offset = 0;
	}
	
	/** 
	 * Writes Records fields in the record string buffer
	 * <br>
	 * If a record field makes the total length of the media record exceed 80 characters, a 
	 * continuation record is created and the next string buffer has two Zero characters in the field
	 * which is used for the record type code.
	 * <br>
	 * The rule for splitting record fields before a continuation records is as follow:
	 * <br>&nbsp;&nbsp;&nbsp;&nbsp; A fixed length field should not be split.
	 * @param string The string to be added to the buffer
	 * @param from The index from which we write the string
	 * @param length Length of the record field.
	 * @throws Exception Throws an exception if a buffer record has not been created
	 * @throws Exception Throws an exception if the field is larger than the specified length
	 *
	 */
	 public void writeRecord(String string,int from, int length) throws Exception {
		if (buff == null) {
				throw new Exception("Buffer Record not created");
		}
		
		if (length > 0) { // fixed length field
				int strLength  = string.length();
				//System.out.println("Length of this String Buffer is " + buff.length());
				//System.out.println("Trying to print " + string + " of length "+strLength 
														 //									 + " in the record which offset is at " + offset); 
				int diffLenth = length - strLength; // diffLength is used to compute the number of leading white space in the fixed length field
				//System.out.println("DiffLength is "+diffLenth);
			
				if (diffLenth < 0) {
					//System.out.println("Error in writeCharAt: Trying to write a string which is longer than the given length ");
					throw new Exception("Trying to print a fixed length field larger than its required length");
				}
				
				buff.insert(from+diffLenth, string); // align right for Numeric and Sign characters.
				//System.out.println("buffer is "+buff);
			}
	 }
	 
	/** 
	 * Writes Records fields in the record string buffer
	 * <br>
	 * If a record field makes the total length of the media record exceed 80 characters, a 
	 * continuation record is created and the next string buffer has two Zero characters in the field
	 * which is used for the record type code.
	 * <br>
	 * The rule for splitting record fields before a continuation records is as follow:
	 * <br>&nbsp;&nbsp;&nbsp;&nbsp; A fixed length field should not be split.
	 * @param string The string to be added to the buffer
	 * @param length Length of the record field. A length = 0 means variable length.
	 * @throws Exception Throws an exception if a buffer record has not been created
	 * @throws Exception Throws an exception if the field is larger than the specified length
	 *
	 */
	 public void writeRecord(String string, int length) throws Exception {
		if (buff == null) {
				throw new Exception("Buffer Record not created");
			}
		
		if (length > 0) { // fixed length field
				int strLength  = string.length();
				//System.out.println("Length of this String Buffer is " + buff.length());
				//System.out.println("Trying to print " + string + " of length "+strLength 
														 //									 + " in the record which offset is at " + offset); 
				int diffLenth = length - strLength; // diffLength is used to compute the number of leading white space in the fixed length field
				//System.out.println("DiffLength is "+diffLenth);
			
				if (diffLenth < 0) {
					//System.out.println("Error in writeCharAt: Trying to write a string which is longer than the given length ");
					throw new Exception("Trying to print a fixed length field larger than its required length");
				}
				
				buff.insert(offset+diffLenth, string); // align right for Numeric and Sign characters.
				//buff.insert(offset, string);  // align left for Alphanumeric, Alphabetic, and Printable characters.
				//System.out.println("buffer is "+buff);
				offset += length;
			}
	 }
	 

  /**
   * Initializes the object from XML tag. <br>
   * <br>
   * @param element source tag
   * @throws Exception Exception if parameters are invalid
   */
  public void load(org.w3c.dom.Element element) throws Exception {
    u.sendNotification(new LoaderNotification(this, u, "Loading CORSIMParser extension"));

    super.load(element);
		
		
		
		String s = element.getAttribute("spatial_model");
    if (s.length()>0) {
      spatialModel = (SpatialModel)u.getExtension(s);
    }
		
		else {
			spatialModel = (SpatialModel)u.getExtension("SpatialModel");
		}
		
		if (spatialModel==null)
      throw new Exception("SpatialModel instance does not exist!");
		
    org.w3c.dom.Node n;
    
    String outName = element.getAttribute("output");
    if (outName.length()>0) {
      o = new java.io.PrintStream(new java.io.FileOutputStream(outName));  
			out = new java.io.PrintWriter(new java.io.BufferedWriter(new java.io.FileWriter(outName)));
		}
		
    u.sendNotification(new LoaderNotification(this, u, "Finished loading CORSIMParser extension"));
  }
}
