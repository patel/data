package eurecom.corsim.records;

public class TrafficRecord {

  private String from = null;
	private String to = null;
	
  public TrafficRecord(String from, String to) {
	  this.from = from;
		this.to = to;
	}
		
	public String getFrom() {
	  return from;
	}
	
	public String getTo() {
	  return to;
	}
}
