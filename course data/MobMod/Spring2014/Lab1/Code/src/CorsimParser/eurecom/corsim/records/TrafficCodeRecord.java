package eurecom.corsim.records;

public class TrafficCodeRecord {

  private String ID = null;
	private int code = 0;
	
  public TrafficCodeRecord(String ID) {
	  this.ID = ID;
		code = 1;
	}
		
	public String getID() {
	  return ID;
	}
	
	public String getCode() {
	  return (String.valueOf(code));
	}
}
