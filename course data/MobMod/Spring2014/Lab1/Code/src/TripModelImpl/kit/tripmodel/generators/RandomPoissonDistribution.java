package kit.tripmodel.generators;

import java.util.Random;

/**
 *
 * A generator of random numbers with Poisson distribution.
 *
 *
 */

public class RandomPoissonDistribution {
  private Random rand;
  private double mean;

  /**
   * Create a new generator of random numbers
   * with Poisson distribution of specified mean.
   *
   * @param mean   the mean of the Poisson distribution
   */
  public RandomPoissonDistribution(double beta) {
    this(beta, new Random());
  }

  /**
   * Create a new generator of random numbers with Poisson
   * distribution of specified mean and that uses <code>randgen</code>
   * as a source of random numbers.
   *
   * @param mean   the mean of the Poisson distribution
   * @param randgen   a Random object to be used by the generator.
   */
  public RandomPoissonDistribution(double mean, Random randgen) {
    this.mean = mean;
    rand = randgen;
  }
	
	/**
   * set the mean of the Poisson distribution
   *
   * @param mean   the mean of the Poisson distribution
   */
  public void setPoissonMean(double mean) {
    this.mean = mean;
  }

  /**
   * Return a random number with Poisson distribution.
   */
  public double nextDouble() {
    // even though a Poisson distribution with mean 10 can be well
    // approximated by a normal distribution, I will use the standard
    // algorithm for means up to 100
    if (mean < 100) {
			// See Knuth, TAOCP, vol. 2, second print
			// section 3.4.1, algorithm Q on page 117
			// Q1. [Calculate exponential]
	
			// In our case, we want the probability of a direction change
	
			double p = Math.exp(-(double)mean);
	
			double N = 0;
			double q = 1.0;

			while (true) {
				// Q2. [Get uniform variable]
				double U = rand.nextDouble();
				// Q3. [Multiply]
				q = q * U;
				// Q4. [Test]
				if (q >= p)
					N = N + 1;
				else
					return N;
			}
    }
    // for larger mean values we approximate the Poisson distribution
    // using a normal distribution
    else {
			double z = rand.nextGaussian();
			double value = (double)(mean + z * Math.sqrt(mean) + 0.5);
			if (value >= 0)
				return value;
			else return 0;
    }
  }


  /**
   * sample usage and testing
   */
  public static void main(String[] args) {
  
    if (args.length == 0) {
      System.out.println("you fool. Gib me the parameters thickhead !!");
      System.exit(-1);
    }
    
    RandomPoissonDistribution poisson 
      = new RandomPoissonDistribution(Double.parseDouble(args[0]));

   // for (int i = 0; i <= Long.parseLong(args[0]); i++)
      System.out.println(poisson.nextDouble());
  }
}
