package de.uni_stuttgart.informatik.canu.mobisim.extensions;

import de.uni_stuttgart.informatik.canu.mobisim.core.*;
import de.uni_stuttgart.informatik.canu.mobisim.simulations.*;
import de.uni_stuttgart.informatik.canu.mobisim.mobilitymodels.Movement;
import de.uni_stuttgart.informatik.canu.mobisim.notifications.*;

/**
 * Title:        Canu Mobility Simulation Environment
 * Description:
 * Copyright:    Copyright (c) 2001-2003
 * Company:      University of Stuttgart
 * <p>	v1.2 (11/06/2007): Added two parameters in order to remove the transcient part and second
 *                         to separate the traces in different chunks of pseudo-steady state.</p>
 * @author v1.1 Canu Research group
 * @author v1.2 Jerome Haerri (haerri@ieee.org), Marco Fiore (fiore@tlc.polito.it)
 * @version 1.2
 */

/**
 * This module displays mobility traces in GlomoSim format
 * @author Illya Stepanov
 */
public class GlomosimOutput extends ExtensionModule
{
  java.io.PrintStream nodePlacement;

  java.io.PrintStream nodeMobility;

  TimeSimulation simulation;

	String nodePositionFile = "nodes.input";
	
	String mobilityFile = "mobility.in";
	
	protected int startLog = 0;
	
	protected int chunkLength = 0;
	
	protected int chunkNumber = 0;
	
	protected boolean firstEntry = true;
	
	protected int startChunkTime = 0;
  
	protected String outName = null;
	
	protected int counter = 0;
	
  /**
   * Constructor
   */
  public GlomosimOutput() throws Exception {
    super("GlomosimOutput");
  }

  /**
   * Returns the module's description. <br>
   * <br>
   * @return module's description
   */
  public String getDescription()
  {
    return "GlomoSim tracing module";
  }

  /**
   * Performs the module initialization. <br>
   * <br>
   * The method is called after finishing the scenario processing.
   */
  public void initialize()
  {
    super.initialize();

    // get TimeSimulation extension
    simulation = (TimeSimulation)u.getExtension("TimeSimulation");
  }

  /**
   * Notification passing method. <br>
   * <br>
   * @param notification notification
   */
  public void sendNotification(Notification notification)
  {
    if (notification instanceof StartingPositionSetNotification)
      outputNotification((StartingPositionSetNotification)notification);
    else
    if (notification instanceof MovementNotification)
      outputNotification((MovementNotification)notification);
  }

  /**
   * Displays the notification in GlomoSim trace format. <br>
   * <br>
   * @param notification notification to display
   */
  public void outputNotification(MovementNotification notification)
  {
    
		try {
		  int currentTime = (int)(u.getTime()/1000f);
		
		  if (currentTime < startLog) 
			  return;
		
		  else if ((chunkLength > 0) && (currentTime >= startLog) && (((startChunkTime - currentTime) % chunkLength) == 0))  {
			  if(firstEntry) {
			    nodeMobility.close();
					nodePlacement.close();
			
				  firstEntry = false; // such that we only open a new file the first time. 
				  chunkNumber++;
					
				  String newTraceName1 = nodePositionFile+String.valueOf(chunkNumber);
					String newTraceName2 = mobilityFile+String.valueOf(chunkNumber);
					
					nodePlacement = new java.io.PrintStream(new java.io.FileOutputStream(newTraceName1));

					nodeMobility = new java.io.PrintStream(new java.io.FileOutputStream(newTraceName2));
				  
				  startChunkTime = currentTime;
			
			
			    // output positions of mobile devices
			    for (int i=0; i<u.getNodes().size(); i++) {
            u.sendNotification(new StartingPositionSetNotification(u.getNodes().get(i), u));
          }
			  }
		  }
		  else  {
			  firstEntry = true;
		  }
		
		
		Node node = (Node)((Movement)notification.getSender()).getOwner();

    int i = u.getNodes().indexOf(node);
		
		if(startChunkTime > 0) {
			nodeMobility.println(i+" "+((u.getTime()/1000f)-(double)startChunkTime)+"S"+
      " ("+node.getPosition().getX()+" "
      +node.getPosition().getY()+" "+node.getPosition().getZ()+")");
		}
		else {
			nodeMobility.println(i+" "+u.getTime()/1000f+"S"+
      " ("+node.getPosition().getX()+" "
      +node.getPosition().getY()+" "+node.getPosition().getZ()+")");
		}
		}
		catch (java.io.FileNotFoundException fnfe) {
			System.out.println("File Not Found Exception in GlomoSimOutput->outputNotification");
			fnfe.printStackTrace();
		}
  }

  /**
   * Displays the notification in GlomoSim trace format. <br>
   * <br>
   * @param notification notification to display
   */
  public void outputNotification(StartingPositionSetNotification notification)
  {
    
		int currentTime = (int)(u.getTime()/1000f);
		
		if (currentTime < startLog) 
			return;
		
		else {
			Node node = (Node)notification.getSender();

			// output position of a mobile node only
			if (node.getExtension("Movement")==null)
				return;

			int i = u.getNodes().indexOf(node);

			nodePlacement.println(i+" 0 ("+node.getPosition().getX()+" "
														+node.getPosition().getY()+" "+node.getPosition().getZ()+")");
		}
  }

  /**
   * Executes the extension. <br>
   * <br>
   * The method is called on every simulation timestep.
   * @return 0 - the module should be executed on next timesteps,
   *        -1 - the module should not be executed on further timesteps and should be removed from the extensions' list
   */
  public int act()
  {
    // check if has to finish soon - GlomoSim fix
		int currentTime = (int)(u.getTime()/1000f);
    if (u.getTime()+u.getStepDuration() >= simulation.getFinishTime() && (currentTime >= startLog)) {
      // output positions of mobile nodes
      for (int i=0; i<u.getNodes().size(); i++) {
        Node node = (Node)u.getNodes().get(i);
        nodeMobility.println(i+" "+u.getTime()/1000f+"S"+
          " ("+node.getPosition().getX()+" "
          +node.getPosition().getY()+" "+node.getPosition().getZ()+")");
     }
    }
    
    return 0;
  }
	/**
    * Initializes the object from XML tag. <br>
    * <br>
    * @param element source tag
    * @throws Exception Exception if parameters are invalid
		* @version 1.2
    */
  public void load(org.w3c.dom.Element element) throws java.lang.Exception
  {
    u.sendNotification(new LoaderNotification(this, u,
      "Loading GlomoSim extension"));

    super.load(element);

    org.w3c.dom.Node n;
		
		String s = null;
		
    s = element.getAttribute("node");
    if (s.length()>0) {
		  nodePositionFile = s;
		}
		s = element.getAttribute("mobility");
    if (s.length()>0) {
		  mobilityFile = s;
		}  

		nodePlacement = new java.io.PrintStream(new java.io.FileOutputStream(nodePositionFile));

    nodeMobility = new java.io.PrintStream(new java.io.FileOutputStream(mobilityFile));

		s = element.getAttribute("startLog");
    if(s.length()>0) {
      startLog = (Integer.valueOf(s).intValue());
			startChunkTime = startLog;
			TimeSimulation timeSimulation = (TimeSimulation)u.getExtension("TimeSimulation");
			if (timeSimulation != null && (chunkLength == 0)) { 
				// we seek the simulation time, as if no chuckLengh is specified, it is set
				// by default to the totalSimulation time+1.
			  chunkLength = ((int)(timeSimulation.getFinishTime()/1000f))+1;
			}
			else
				 throw new Exception("TimeSimulation MUST be loaded before GlomoSimOutput");
		}
		s = element.getAttribute("chunkLength");
    if(s.length()>0) {
      chunkLength = (Integer.valueOf(s).intValue());
		}
		
    u.addNotificationListener(this);

    u.sendNotification(new LoaderNotification(this, u,
      "Finished loading GlomoSim extension"));
  }
	
	
}