package de.uni_stuttgart.informatik.canu.mobisim.extensions;

import de.uni_stuttgart.informatik.canu.mobisim.core.*;
import de.uni_stuttgart.informatik.canu.mobisim.notifications.*;
import de.uni_stuttgart.informatik.canu.mobisim.mobilitymodels.*;
import de.uni_stuttgart.informatik.canu.mobisim.simulations.*;
/**
 * Title:        Canu Mobility Simulation Environment
 * Description:
 * Copyright:    Copyright (c) 2001-2004
 * Company:      University of Stuttgart
 * <p>	v1.2 (11/06/2007): Added two parameters in order to remove the transcient part and second
 *                         to separate the traces in different chunks of pseudo-steady state.Also,
 *                         the "#" in front of each nodeID has been removed</p>
 * @author v1.1 Canu Research group
 * @author v1.2 Jerome Haerri (haerri@ieee.org), Marco Fiore (fiore@tlc.polito.it)
 * @version 1.2
 */

/**
 * This module reports periodically positions of mobile nodes
 * @author Illya Stepanov
 */
public class ReportNodeMobility extends ExtensionModule
{
  /**
   * Step of reporting (in ms)
   */
  protected int step;

  /**
   * Output Stream
   */
  protected java.io.PrintStream o = System.err;
	
	
	protected int startLog = 0;
	
	protected int chunkLength = 0;
	
	protected int chunkNumber = 0;
	
	protected boolean firstEntry = true;
	
	protected int startChunkTime = 0;
  
	protected String outName = null;
	
	protected int counter = 0;
	
  /**
   * Constructor
   */
  public ReportNodeMobility()
  {
    super("ReportNodeMobility");
  }

  /**
   * Executes the extension. <br>
   * <br>
   * The method is called on every simulation timestep. 
   * @return 0 - the module should be executed on next timesteps,
   *        -1 - the module should not be executed on further timesteps and should be removed from the extensions' list
   */
  public int act()
  {
    Universe u = Universe.getReference();
		
		int currentTime = (int)(u.getTime()/1000f);
    if ( (u.getTime() % step == 0) && (currentTime >= startLog) ) {
			activate();
    }
    
    return 0;
  }

  /**
   * Dumps node positions
   */
  public void activate() {
		
		try {
		 Universe u = Universe.getReference();
     int currentTime = (int)(u.getTime()/1000f);
		 
		
		 if ((chunkLength > 0) && (currentTime >= startLog) && (((startChunkTime - currentTime) % chunkLength) == 0))  {
			 
			 if(firstEntry) {
			    o.close();
				  firstEntry = false; // such that we only open a new file the first time. 
				  chunkNumber++;
				  String newTraceName = outName+String.valueOf(chunkNumber);
					System.out.println("newTraceFile is "+newTraceName);
				  o = new java.io.PrintStream(new java.io.FileOutputStream(newTraceName)); 
				  
					o.println("#");
				  o.println("# nodes: "+u.getNodes().size()+", pause: "+Float.MAX_VALUE+", max speed: "+Float.MAX_VALUE+"  max x = "+u.getDimensionX()+", max y: "+u.getDimensionY());
				  o.println("#");
		
				  startChunkTime = currentTime;
				}
			}
			else  {
			  firstEntry = true;
		  }
		
		// dump node positions
    java.util.Iterator iter = u.getNodes().iterator();
    
		while (iter.hasNext())
    {
      Node node = (Node)iter.next();
      Movement movement = (Movement)node.getExtension("Movement");
      int i = u.getNodes().indexOf(node);
			
			if(startChunkTime > 0) {
		      o.println(i+" "+(((float)u.getTime()/1000) - (float)startChunkTime)+" "+
                node.getPosition().getX()+" "+node.getPosition().getY()+" "+
                movement.getSpeed()*1000);
			}
			else {
			    o.println(i+" "+(float)u.getTime()/1000+" "+
                node.getPosition().getX()+" "+node.getPosition().getY()+" "+
                movement.getSpeed()*1000);
			}
    }
	}
	 catch (java.io.FileNotFoundException fnfe) {
			System.out.println("File Not Found Exception in ReportNodeMobility->outputNotification");
			fnfe.printStackTrace();
	 }
  }

  /**
   * Returns the module's description. <br>
   * <br>
   * @return extension module's description
   */
  public String getDescription()
  {
    return "Node positions and speeds reporting module";
  }

  /**
    * Initializes the object from XML tag. <br>
    * <br>
    * @param element source tag
    * @throws Exception Exception if parameters are invalid
    */
  public void load(org.w3c.dom.Element element) throws java.lang.Exception
  {
    u.sendNotification(new LoaderNotification(this, u,
      "Loading ReportNodeMobility extension"));

    super.load(element);

    org.w3c.dom.Node n;

    outName = element.getAttribute("output");
    if (outName.length()>0)
      o = new java.io.PrintStream(new java.io.FileOutputStream(outName));    

		String s = element.getAttribute("startLog");
    if(s.length()>0) {
      startLog = (Integer.valueOf(s).intValue());
			startChunkTime = startLog;
			TimeSimulation timeSimulation = (TimeSimulation)u.getExtension("TimeSimulation");
			if (timeSimulation != null && (chunkLength == 0)) { 
				// we seek the simulation time, as if no chuckLengh is specified, it is set
				// by default to the totalSimulation time+1.
			  chunkLength = ((int)(timeSimulation.getFinishTime()/1000f))+1;
			}
			else
				 throw new Exception("TimeSimulation MUST be loaded before ReportNodeMobility");
		}
		s = element.getAttribute("chunkLength");
    if(s.length()>0) {
      chunkLength = (Integer.valueOf(s).intValue());
		}
		
    n=element.getElementsByTagName("step").item(0);
    if(n==null)
      throw new Exception("<step> is missing!");
    step=(int)(Float.parseFloat(n.getFirstChild().getNodeValue())*1000.0f);

    // checkout
    if (step<=0)
      throw new Exception("Step value is invalid: "+step);

    u.sendNotification(new LoaderNotification(this, u,
      "Finished loading ReportNodeMobility extension"));
  }
}
