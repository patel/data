package de.uni_stuttgart.informatik.canu.mobisim.extensions;

import de.uni_stuttgart.informatik.canu.mobisim.core.*;
import de.uni_stuttgart.informatik.canu.mobisim.notifications.*;
import de.uni_stuttgart.informatik.canu.mobisim.simulations.*;
import de.uni_stuttgart.informatik.canu.mobisim.mobilitymodels.Movement;


/**
 * Title:        Canu Mobility Simulation Environment
 * Description:
 * Copyright:    Copyright (c) 2001-2003
 * Company:      University of Stuttgart
 * <p>Patches: </p>
 * <p>	v1.2 (10/05/2007): Added two parameters in order to remove the transcient part and second
 *                         to separate the traces in different chunks of pseudo-steady state.</p>
 * @author v1.1 Canu Research group
 * @author v1.2 Jerome Haerri (haerri@ieee.org), Marco Fiore (fiore@tlc.polito.it)
 * @version 1.2
 */

/**
 * This module displays mobility traces in NS-2 format
 * @author Illya Stepanov
 */
public class NSOutput extends ExtensionModule
{
  /**
   * Output Stream
   */
  protected java.io.PrintStream o = System.out;
	
	protected int startLog = 0;
	
	protected int chunkLength = 0;
	
	protected int chunkNumber = 0;
	
	protected boolean firstEntry = true;
	
	protected int startChunkTime = 0;
  
	protected String outName = null;
	
	protected int counter = 0;
	
  /**
   * Constructor
   */
  public NSOutput()
  {
    super("NSOutput");
  }

  /**
   * Returns the module's description. <br>
   * <br>
   * @return extension module's description
   */
  public String getDescription()
  {
    return "NS-2 tracing module";
  }

  /**
   * Performs the module initialization. <br>
   * <br>
   * The method is called after finishing the scenario processing.
   */
  public void initialize()
  {
    super.initialize();
    
    // write scenario information
    o.println("#");
    o.println("# nodes: "+u.getNodes().size()+", pause: "+Float.MAX_VALUE+", max speed: "+Float.MAX_VALUE+"  max x = "+u.getDimensionX()+", max y: "+u.getDimensionY());
    o.println("#");
  }

  /**
   * Notification passing method. <br>
   * <br>
   * @param notification notification
   */
  public void sendNotification(Notification notification)
  {
    if (notification instanceof StartingPositionSetNotification)
      outputNotification((StartingPositionSetNotification)notification);
    else
    if (notification instanceof MovementChangedNotification)
      outputNotification((MovementChangedNotification)notification);
  }

  /**
   * Displays the notification in NS-2 format. <br>
   * <br>
   * @param notification notification to display
   */
  public void outputNotification(MovementChangedNotification notification) {
    
		try {
		  int currentTime = (int)(u.getTime()/1000f);
		
		  if (currentTime < startLog) 
			  return;
		
		  else if ((chunkLength > 0) && (currentTime >= startLog) && (((startChunkTime - currentTime) % chunkLength) == 0))  {
			  if(firstEntry) {
			    o.close();
				
				  firstEntry = false; // such that we only open a new file the first time. 
				  chunkNumber++;
				  String newTraceName = outName+String.valueOf(chunkNumber);
				  o = new java.io.PrintStream(new java.io.FileOutputStream(newTraceName)); 
				  
					o.println("#");
				  o.println("# nodes: "+u.getNodes().size()+", pause: "+Float.MAX_VALUE+", max speed: "+Float.MAX_VALUE+"  max x = "+u.getDimensionX()+", max y: "+u.getDimensionY());
				  o.println("#");
		
				  startChunkTime = currentTime;
			
			
			    // output positions of mobile devices
			    for (int i=0; i<u.getNodes().size(); i++) {
            u.sendNotification(new StartingPositionSetNotification(u.getNodes().get(i), u));
          }
			  }
		  }
		  else  {
			  firstEntry = true;
		  }
		
		  Position3D destination = notification.getDestination();
      Node node = (Node)((Movement)notification.getSender()).getOwner();

      int i = u.getNodes().indexOf(node);
      
			if(startChunkTime > 0) {
			  o.println("$ns_ at "+((u.getTime()/1000f)-(double)startChunkTime)+
          " \"$node_("+i+") setdest "+
          (destination.getX()+0.000001)+" "+
          (destination.getY()+0.000001)+" "+
          notification.getSpeed()+"\"");
			}
			else {
        o.println("$ns_ at "+u.getTime()/1000f+
          " \"$node_("+i+") setdest "+
          (destination.getX()+0.000001)+" "+
          (destination.getY()+0.000001)+" "+
          notification.getSpeed()+"\"");
			}
		}
		catch (java.io.FileNotFoundException fnfe) {
			System.out.println("File Not Found Exception in NSOutput->outputNotification");
			fnfe.printStackTrace();
		}
  }

  /**
   * Displays the notification in NS-2 format. <br>
   * <br>
   * @param notification notification to display
   */
  public void outputNotification(StartingPositionSetNotification notification) {
    
		int currentTime = (int)(u.getTime()/1000f);
		
		if (currentTime < startLog) 
			return;
		
		else {
				
			Node node = (Node)notification.getSender();

		  // output position of mobile nodes only
		  if (node.getExtension("Movement")==null)
			  return;

		  int i = u.getNodes().indexOf(node);

			o.println("$node_("+i+") set X_ "+ (node.getPosition().getX()+0.000001));
			o.println("$node_("+i+") set Y_ "+ (node.getPosition().getY()+0.000001));
			o.println("$node_("+i+") set Z_ "+node.getPosition().getZ());
			
		}
  }

  /**
   * Executes the extension. <br>
   * <br>
   * The method is called on every simulation timestep.
   * @return 0 - the module should be executed on next timesteps,
   *        -1 - the module should not be executed on further timesteps and should be removed from the extensions' list
   */
  public int act()
  {
		return 0;
  }
  
  /**
    * Initializes the object from XML tag. <br>
    * <br>
    * @param element source tag
    * @throws Exception Exception if parameters are invalid
    */
  public void load(org.w3c.dom.Element element) throws java.lang.Exception
  {
    u.sendNotification(new LoaderNotification(this, u,
      "Loading NSOutput extension"));

    super.load(element);

    org.w3c.dom.Node n;
		
    outName = element.getAttribute("output");
    if (outName.length()>0)
      o = new java.io.PrintStream(new java.io.FileOutputStream(outName));    

		String s = element.getAttribute("startLog");
    if(s.length()>0) {
      startLog = (Integer.valueOf(s).intValue());
			startChunkTime = startLog;
			TimeSimulation timeSimulation = (TimeSimulation)u.getExtension("TimeSimulation");
			if (timeSimulation != null && (chunkLength == 0)) { 
				// we seek the simulation time, as if no chuckLengh is specified, it is set
				// by default to the totalSimulation time+1.
			  chunkLength = ((int)(timeSimulation.getFinishTime()/1000f))+1;
			}
			else
				 throw new Exception("TimeSimulation MUST be loaded before NSOutput");
		}
		s = element.getAttribute("chunkLength");
    if(s.length()>0) {
      chunkLength = (Integer.valueOf(s).intValue());
		}
		
    u.addNotificationListener(this);

    u.sendNotification(new LoaderNotification(this, u,
      "Finished loading NSOutput extension"));
  }
}